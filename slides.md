title: Software und Forschungsdaten unter Kontrolle mit Git
layout: true
name: nfdi4ing-template
class: nfdi4ing
<div class="slide-layout">
    <div class="slide-header"></div>
    <div class="slide-footer">01.06.2021 — Seite</div>
    <a class="slide-license" href="https://creativecommons.org/licenses/by-sa/4.0" target="_blank" title="CC-BY-SA 4.0"></a>
</div>

---

<!-- class: title-green -->

# Software und Forschungsdaten unter Kontrolle mit Git

Cord Wiljes

01.06.2021

---
<!-- class: sparse -->

# Themen

* Grundlagen der Versionierung
* Git in der Shell
* Branching und Merges
* Versionskonflikte lösen
* Git Large File Storage
* Git mit Dektop-Client oder IDE nutzen

---

# Git

<img src="img/xkcd-comic.png" height="450">

Source: <a href="https://xkcd.com/1597/">https://xkcd.com/1597/</a> 



---

# Erfahrungen mit der Vorbereitung

s. https://cordwiljes.gitlab.io/git-workshop-2021-06-01 

1. persönlichen GitLab-Account auf dem GitLab Ihrer Institution aktivieren
2. Git auf ihrem likalen Rechner installieren
```bash
git --version
```
3. Ein erstes Projekt anlegen und klonen
```bash
mkdir git-workshop
cd git-workshop
git clone [URL]
```
4. Git Desktop-Client installieren (optional)

---

# Versionierung

<img src="img/phdcomics-versioning.png" height="450">

Source: <a href="http://phdcomics.com/comics/archive_print.php?comicid=1531 
">http://phdcomics.com/comics/archive_print.php?comicid=1531</a> 

---

# Versionierung: lokal 

<img src="img/versionierung-1.png">

---

# Versionierung: zentral 

<img src="img/versionierung-2.png">

---

# Versionierung: verteilt 

<img src="img/versionierung-3.png" height="500">

---

# Git Versionierungsworkflow 

<img src="img/versionierung.png">

---

# Zusammenspiel von Git und GitLab

<img src="img/gitlab-git.png" width="700">

---

# Software Carpentry Course

https://swcarpentry.github.io/git-novice/

---

<!-- class: small -->

# Grundlegende Befehle: Globale Konfiguration

## Konfiguration nur einmalig pro Computer erforderlich:

Konfiguriert `<Benutername>` als globalen Git-Benutzernamen
```bash
git config --global user.name <Benutzername>
# z.B.
git config --global user.name "Cord Wiljes"
```

Konfiguriert `<Email-Adresse>` als globale Git-Emailadresse
```bash
git config --global user.email <Email-Adresse>
# z.B.
git config --global user.email "cord@wiljes.de"
```

---

# Grundlegende Befehle: Lokales Repository

Initialisiert im aktuellen Verzeichnis ein git Repository
```bash
git init
```

Fügt Dateien dem staging Bereich hinzu
```bash
git add <Dateiname>
# z.B.
git add *
```

Commitet aus dem Staging ins Repository
```bash
git commit -m <commit message>
#z.B.
git commit –m "Erster Commit"
```

---

# Grundlegende Befehle: Remote Repository (1)

Klont ein entferntes Repository
```bash
git clone <repository url>
# z.B.
git clone https://git.rwth-aachen.de/grp/repo.git
```

Versionshistorie an das entfernte Repository senden
```bash
git push <repository url> <branch>
# z.B.
git push https://clarin06.ims.uni-stuttgart.de/gitlab-git-schulung/fragen.git master
# das origin Repository
git push origin master
# oder den aktuellen branch zum default remote
git push
```

---

# Grundlegende Befehle: Remote Repository (2)

Holt den Versionsstand aus dem entfernten Repository und mergt in das Arbeitsverzeichnis
```bash
git pull <repository url> <branch>
# z.B.
git pull https://clarin06.ims.uni-stuttgart.de/gitlab-git-schulung/fragen.git master
# oder den aktuellen branch vom default remote
git pull
```

---

<!-- class: title-green -->

# Übung: Git Tac Toe

---

<!-- class: img-right -->

# Übung: Git Tac Toe - Regeln

![Tic Tac Toe](img/tictactoe.svg)

* 2 Spieler: `X` und `O`
* Auf einem Feld 3x3
* Spieler ziehen abwechselnd
* In jedem Zug setzt der Spieler sein Symbol in ein Feld
* Gewonnen hat, wer drei Symbole in einer Zeile, Spalte oder Diagonale hat

In einer Textdatei sieht das Spielfeld dann so aus:

```python
 | | 
-+-+-
 | | 
-+-+-
 | | 
```

---

# Übung: Git Tac Toe - *Runde 1 Abwechselnd (1)*

Vorbereitung: Teilen Sie sich in 2er Gruppen mit `X` und `O`

> `X` erstellt ein GitLab Repository und initialisiert das Projekt mit einer Readme.md

> `X` lädt `O` als *Maintainer* in das Projekt ein

Vorbereitung: Beide Spieler klonen das Repository auf ihren Rechnern

Vorbereitung: `X` legt eine `Runde1.txt` mit dem Spielfeld an

```bash
X> git add Runde1.txt
X> git commit -m "Spielfeld für Runde 1"
```

---

<!-- class: dense -->

# Übung: Git Tac Toe - *Runde 1 Abweschselnd (2)* 

Spielzug: `X` macht den ersten Zug im Spielfeld
```bash
X> git add Runde1.txt
X> git commit -m "X Zug 1"
X> git push
```

Spielzug: `O` aktualisiert das Repository und macht den nächsten Zug

```bash
O> git pull
```

> `O` macht den Zug im Spielfeld

```bash
O> git add Runde1.txt
O> git commit -m "O Zug 1"
O> git push
```
Spielzug: `X` ist an der Reihe und startet mit git pull und dem zweiten Zug

... und so weiter ...

---

# Übung: Git Tac Toe - *Runde 2 Gleichzeitig (1)*

Vorbereitung: `X` erstellt eine neue Datei `Runde2.txt`, füllt diese mit dem Spielfeld

```bash
X> git add Runde2.txt
X> git commit -m "Spielfeld für Runde 2"
X> git push
```

Spielzug gleichzeitig: `X` und `O` machen ihre Züge

```bash
X&O> git pull
```

> Zug im Spielfeld eintragen

```bash
X&0> git add Runde2.txt
X&O> git commit –m "{X,O} Zug 1"
```

---

# Übung: Git Tac Toe - *Runde 2 Gleichzeitig (2)*

Spielzug nacheinander: Änderungen an den Server senden (Erst Spieler `O`!)

```bash
X&O> git push
```

> *Der push von X wird rejected!*

`X` muss den Konflikt lösen:

```bash
X> git pull
```

> Entweder: git macht einen automatischen merge
> Oder: `X` öffnet `Runde2.txt` und führt die Spielstände zusammen.

```bash
X> git add Runde2.txt
X> git commit -m "Zug 1 zusammengeführt"
X> git push
```
... und so weiter ... 

Machen Sie noch eine Runde 3 mit getauschten Rollen `X` und `O`

---

# Übung: Git Tac Toe - *Runde 3 Branches (1)*

Vorbereitung: `X` erstellt eine neue Datei `Runde3.txt` und füllt diese mit dem Spielfeld

```bash
X> git add Runde4.txt
X> git commit -m "Spielfeld für Runde 3"
X> git push
```

Vorbereitung: `O` aktualisiert das Repository: 

```bash
O> git pull
```

Vorbereitung: Beide erstellen einen Branch für ihre Spielzüge:

```bash
X&O> git checkout -b "r4s{X,O}"
X&O> git push --set-upstream origin r4s{X,O}
```

`X` und `O` spielen jeweils auf dem eigenen Branch.

---

# Übung: Git Tac Toe - *Runde 3 Branches (2)*

Spielzug gleichzeitig: `X` und `O` machen den Spielzug und senden Änderungen an den Server

> Branches aktualisieren

```bash
X&O> git pull
```

> Zug im Spielfeld eintragen

```bash
X&O> git add Runde4.txt
X&O> git commit –m "{X,O} Zug 1"
X&O> git push
```

---

# Übung: Git Tac Toe - *Runde 3 Branches (3)*

Spielzug `X`: Führt die Änderungen zusammen

```bash
git fetch
git merge origin/r4sO
```

> `X` führt Spielstände zusammen

```bash
git add Runde4.txt
git commit -m "Zug 1 zusammengeführt"
git push
```

Spielzug `O`: Holt Änderungen von `X`

```bash
git fetch
git merge origin/r4sX
```

... und so weiter ...

Machen Sie noch eine Runde mit getauschten Rollen `X` und `O`


---
<!-- class: wide -->

<p style="margin-top:-3em">
.center[
![Git Cheat Sheet by by Hylke Bons based on work by Zack Rusin and Sébastien Pierre. This work is licensed under the Creative Commons Attribution 3.0 License.](https://raw.githubusercontent.com/hbons/git-cheat-sheet/master/preview.png)
] 

https://github.com/hbons/git-cheat-sheet


---
# Git Large File Storage (LFS)

* Problem: empfohlene maximale Größe eines Repositories: 1GB 
* Lösung: Git Large File Storage (LFS) ersetzt große Dateien wie Videos etc. durch Textzeiger innerhalb von Git.

## Git LFS initialisieren:
```bash
git lfs install
```

## Dateien für LSF auswählen:
```bash 
git lfs track "*.iso"
```

## .gitattributes tracken:
```bash
git add .gitattributes
```

---
<!-- class: sparse, img-right -->

# Abschluss

* Fragen
* Anwendung in der eigenen Forschung
* weiteres Vorgehen

---
<!-- class: sparse, img-right -->

# Kontakt

Cord Wiljes

E-Mail: [support@wiljes.de](mailto:support@wiljes.de)

Tel.: 0521-5217-602

